package com.amplify;

import junit.framework.TestCase;

public class VideoStoreTest extends TestCase {
  private Customer customer;

  public VideoStoreTest(String name) {
    super(name);
  }

  protected void setUp() {
    customer = new Customer("Customer Name");
  }

  private void assertAmountAndPointsForReport(double expectedAmount, int expectedPoints) {
    assertEquals(expectedAmount, customer.getAmountOwed());
    assertEquals(expectedPoints, customer.getFrequentRenterPoints());
  }

  public void testSingleNewReleaseStatement() {
      customer.addRental(new Rental(new Movie("The Hustle", Movie.NEW_RELEASE), 3));
      customer.statement();
    assertAmountAndPointsForReport(9.0, 2);
  }

  public void testDualNewReleaseStatement() {
      customer.addRental(new Rental(new Movie("The Hustle", Movie.NEW_RELEASE), 3));
      customer.addRental(new Rental(new Movie("Gravity", Movie.NEW_RELEASE), 3));
      customer.statement();
    assertAmountAndPointsForReport(18.0, 4);
  }

  public void testSingleChildrensStatement() {
      customer.addRental(new Rental(new Movie("Frozen", Movie.CHILDRENS), 3));
      customer.statement();
    assertAmountAndPointsForReport(1.5, 1);
  }


  public void testMultipleRegularStatement() {
      customer.addRental(new Rental(new Movie("No Country for Old Men", Movie.REGULAR), 1));
      customer.addRental(new Rental(new Movie("O Brother, Where Art Thou?", Movie.REGULAR), 2));
      customer.addRental(new Rental(new Movie("Barton Fink", Movie.REGULAR), 3));
      customer.statement();
    assertAmountAndPointsForReport(7.5, 3);
  }

  public void testRentalStatementFormat() {
      customer.addRental(new Rental(new Movie("No Country for Old Men", Movie.REGULAR), 1));
      customer.addRental(new Rental(new Movie("O Brother, Where Art Thou?", Movie.REGULAR), 2));
      customer.addRental(new Rental(new Movie("Barton Fink", Movie.REGULAR), 3));

    assertEquals(
      "Rental Record for Customer Name\n" +
        "\tNo Country for Old Men\t2.0\n" +
        "\tO Brother, Where Art Thou?\t2.0\n" +
        "\tBarton Fink\t3.5\n" +
        "You owed 7.5\n" +
        "You earned 3 frequent renter points\n",
            customer.statement());
  }
}