package com.amplify;

public class Movie
{
    public static final int CHILDRENS = 2;
    public static final int REGULAR = 0;
    public static final int NEW_RELEASE  = 1;
    private int _priceCode;

    // The title of the movie
	private String title;

    // constructor
	public Movie(String title, int _priceCode) {
		this.title 		= title;
        this._priceCode = _priceCode;
	}

	public String getTitle () {
		return title;
	}

    public int getPriceCode() {
        return this._priceCode;
    }

    public void set_priceCode(int arg) {
        _priceCode = arg;
    }

}